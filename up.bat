echo starting window1 and window2 servers
start "demo-window1" http-server ./window1 -p8765
start "demo-window2" http-server ./window2 -p8764
echo press to kill servers and end demo
pause
rem taskkill /im demo-window1
rem taskkill /im demo-window2
rem tasklist /V /FI "WindowTitle eq demo-window1*"
rem tasklist /V /FI "WindowTitle eq demo-window2*"
taskkill /FI "WindowTitle eq demo-window1*" /T /F
taskkill /FI "WindowTitle eq demo-window2*" /T /F
